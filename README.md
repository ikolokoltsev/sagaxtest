# SAGAXTest
   
   this is a test task for SAGAX. So let's go make some cools things and get a lot of fun with this small proj 😎

## Work to be done:

   - We need to have a nice designed app. We want to have dark and light theme, and it should be switched easily.
   - Also we want to change icons, and it should connected with theme. We should me able to change size, color, etc.

## Nice to have:

   - Fonts! Yes, we want to be able change fonts the same way as we do it with icons
   - Don't forget about background. We want to change background color, and use images from the gallery as a background for our app.

## App dev plan:

   - First of all I want to replace npm with pnpm, because pnpm as pnpm says "I'm better, I'm smarter, I'm stronger!" (pnpm never said that 😅)
   - Then I will pick UI library to setup theme.
   - And then I'll go thought the "Work to be done" and "Nice to have" lists.(so this chapter will be updated in future)
